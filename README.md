# spring-testng-parallel

Demo Project to show Parallelism with Spring and TestNG using testng groups


```bash
mvn clean -U test -Dgroups=smokeTest
```

```bash
allure serve target/allure-results
```
