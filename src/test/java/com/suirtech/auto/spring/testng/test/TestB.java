package com.suirtech.auto.spring.testng.test;

import com.suirtech.auto.spring.testng.TestConfig;
import org.apache.log4j.Logger;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.Test;

import java.util.Random;

@ContextConfiguration(classes = TestConfig.class)
public class TestB extends AbstractTestNGSpringContextTests {

    Logger logger = Logger.getLogger(TestA.class);


    @Test
    public void testB1() {
        int s = waitTest();
        long threadId = Thread.currentThread().getId();
        logger.info("*****************************");
        logger.info("im testB1 :: TID : " + threadId + " slept for " + s);
        logger.info("*****************************");
    }


    @Test(groups = {"smokeTest"})
    public void testB2() {
        int s = waitTest();
        long threadId = Thread.currentThread().getId();
        logger.info("*****************************");
        logger.info("im testB2 :: TID : " + threadId + " slept for " + s);
        logger.info("*****************************");
    }


    @Test(groups = {"regression"})
    public void testB3() {
        int s = waitTest();
        long threadId = Thread.currentThread().getId();
        logger.info("*****************************");
        logger.info("im testB3 :: TID : " + threadId + " slept for " + s);
        logger.info("*****************************");
    }


    private int waitTest() {
        int low = 1;
        int high = 8;
        int t = new Random().nextInt(high - low) + low;

        try {
            logger.info("Sleepign for " + t);
            Thread.sleep(t * 1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return t;
    }
}
