package com.suirtech.auto.spring.testng.test;

import com.suirtech.auto.spring.testng.TestConfig;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.Test;

import java.util.Random;

@ContextConfiguration(classes = TestConfig.class)
public class TestC extends AbstractTestNGSpringContextTests {

    @Test(groups = {"smokeTest"})
    public void testC1() {
        int s = waitTest();
        long threadId = Thread.currentThread().getId();
        logger.info("*****************************");
        logger.info("im testC1 :: TID : " + threadId + " slept for " + s);
        logger.info("*****************************");
    }


    @Test(groups = {"smokeTest"})
    public void testC2() {
        int s = waitTest();
        long threadId = Thread.currentThread().getId();
        logger.info("*****************************");
        logger.info("im testC2 :: TID : " + threadId + " slept for " + s);
        logger.info("*****************************");
    }


    @Test(groups = {"regression"})
    public void testC3() {
        int s = waitTest();
        long threadId = Thread.currentThread().getId();
        logger.info("*****************************");
        logger.info("im testC3 :: TID : " + threadId + " slept for " + s);
        logger.info("*****************************");
    }


    private int waitTest() {
        int low = 1;
        int high = 8;
        int s = new Random().nextInt(high - low) + low;

        try {
            logger.info("Sleepign for " + s);
            Thread.sleep(s * 1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return s;
    }
}
