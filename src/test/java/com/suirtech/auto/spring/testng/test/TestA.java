package com.suirtech.auto.spring.testng.test;

import com.suirtech.auto.spring.testng.TestConfig;
import org.apache.log4j.Logger;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.Test;

import java.util.Random;

@ContextConfiguration(classes = TestConfig.class)
public class TestA extends AbstractTestNGSpringContextTests {

    Logger logger = Logger.getLogger(TestA.class);


    @Test(groups = {"smokeTest"})
    public void testA1() {
        int s = waitTest();
        long threadId = Thread.currentThread().getId();
        logger.info("*****************************");
        logger.info("im testA1 :: TID : " + threadId + " slept for " + s);
        logger.info("*****************************");

    }

    private int waitTest() {
        Random r = new Random();
        int low = 2;
        int high = 11;
        int k = r.nextInt(high - low) + low;
        try {
            logger.info("Sleepign for " + k);
            Thread.sleep(k * 1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return k;
    }

}
